Super Theme v3
==================

**Theme Name:** Super Theme v3

**Theme Description:** Theme for Super Theme v3.

**Developers name(s):** Adam Glenn

**Bitbucket repository URL:** [https://bitbucket.org/wvudigital/ur-super-theme-v2](https://bitbucket.org/wvudigital/ur-super-theme-v2)

**Dependencies necessary to work with this theme:** Sass.



## Gulp & Super Theme

**Requirements**
* [NodeJS](https://nodejs.org)

You will need to install Node ^8.9.4.

  1. Download and install NodeJS from https://nodejs.org/en if you haven't already.
  1. Install Gulp globally by entering `npm install -g gulp` in your terminal.
  1. Navigate to your project's directory via terminal (something like `cd ~/Sites/cleanslate_themes/MY-SITE`)
  1. Install node modules by typing `npm install`
  1. Run Gulp by typing `gulp`.

**Note:** the `gulpfile.js` in its base form will only compile your Sass.
